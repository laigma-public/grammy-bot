import pg from "pg";
import customEnv from "custom-env";

customEnv.env(process.env.NODE_ENV);

const pool = new pg.Pool({
  user: process.env.USER,
  host: process.env.HOST,
  database: process.env.DATABASE,
  password: process.env.PASSWORD,
  port: process.env.PORT,
});

export default {
  async execQuery(query) {
    return new Promise((resolve) => {
      pool.query(query, (error, results) => {
        if (error) {
          throw error
        }
        resolve(results.rows)
      })
    });
  }
};