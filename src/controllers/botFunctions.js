import { readFile } from "fs/promises";
import dataFunctions from "./dataFunctions.js";
import { sendMessageToAll } from "../bot.js";

const packageJson = await readFile(new URL('../../package.json', import.meta.url))
const info = JSON.parse(packageJson);

export default {
  welcomeMessage(ctx) {
    const message = `Bienvenido <b>@${ctx.msg.from.username}</b>\n<b><i>GrammY Bot v${info.version}</i></b>`;
    ctx.reply(message, { parse_mode: "HTML" });
  },

  async unsubscribe(ctx) {
    const resLog = await dataFunctions.deleteAdmin(ctx.msg.from);
    ctx.reply(resLog);
  },

  async sendAdminList(ctx) {
    const adminList = await dataFunctions.getAdminList();
    const userData = adminList.find(e => parseInt(e.id) === ctx.msg.from.id);
    if (!userData) {
      ctx.reply("No tienes permisos para ejecutar este comando");
      return;
    }
    const message = `Lista de usuarios:\n${adminList.map(item => `@${item.username}`).join('\n')}`;
    ctx.reply(message);
  },

  async replyMessage(ctx) {
    const adminList = await dataFunctions.getAdminList();
    const userData = adminList.find(e => parseInt(e.id) === ctx.msg.from.id);
    if (!userData) {
      ctx.reply("No tienes permisos para interactuar. Debes registrarte en el bot.");
      return;
    }
    await sendMessageToAll(`<b>@${ctx.msg.from.username} > ${ctx.msg.text}</b>`, { parse_mode: "HTML" });
  },
}